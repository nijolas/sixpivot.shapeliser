﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixPivot.Shapeliser.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SixPivot.Shapeliser.Tests.Core
{
    [TestClass]
    [TestCategory("Unit tests")]
    public class TokeniserTests
    {
        private Tokeniser tokeniser;

        [TestInitialize]
        public void Intialise()
        {
            this.tokeniser = new Tokeniser();
        }

        void Assert_TokensIsNotNullAndHasCount(IEnumerable<Token> tokens, int count)
        {
            Assert.IsNotNull(tokens, "Expected tokens to be non-null.");
            Assert.AreEqual(count, tokens.Count(), $"Expected Tokens to contain {count}, got {tokens.Count()}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [DataRow(" ")]
        public void Tokenise_GivenEmptyInput_ReturnsEmptyTokensAndEmptyErrors(string value)
        {
            // arrange
            // act
            var result = this.tokeniser.Tokenise(value);

            // assert
            Assert_TokensIsNotNullAndHasCount(result, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(InputTooLongException))]
        public void Tokenise_GivenExcessivelyLongInput_ThrowsInputTooLongException()
        {
            // arrange
            var longInput = String.Concat(Enumerable.Repeat("Hello", 100));

            // act, assert
            this.tokeniser.Tokenise(longInput);
        }

        [TestMethod]
        public void Tokenise_GivenSingleWord_ReturnsSingleWordToken()
        {
            // arrange
            // act
            var result = this.tokeniser.Tokenise("word");

            // assert
            Assert_TokensIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_TokenHasValueAndPosition(result.First(), "word", 0);
        }

        [TestMethod]
        public void Tokenise_GivenTwoWords_ReturnsTwoWordTokens()
        {
            // arrange
            // act
            var result = this.tokeniser.Tokenise("word word2");

            // assert
            Assert_TokensIsNotNullAndHasCount(result, 2);
            AssertCommon.Assert_TokenHasValueAndPosition(result.First(), "word", 0);
            AssertCommon.Assert_TokenHasValueAndPosition(result.Last(), "word2", 5);
        }

        [TestMethod]
        public void Tokenise_GivenSingleNumber_ReturnsSingleNumberToken()
        {
            // arrange
            // act
            var result = this.tokeniser.Tokenise("123");

            // assert
            Assert_TokensIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_TokenHasValueAndPosition(result.First(), "123", 0);
        }

        [TestMethod]
        public void Tokenise_GivenTwoNumbers_ReturnsTwoNumberTokens()
        {
            // arrange
            // act
            var result = this.tokeniser.Tokenise("123 456");

            // assert
            Assert_TokensIsNotNullAndHasCount(result, 2);
            AssertCommon.Assert_TokenHasValueAndPosition(result.First(), "123", 0);
            AssertCommon.Assert_TokenHasValueAndPosition(result.Last(), "456", 4);
        }

        [TestMethod]
        public void Tokenise_GivenNumberWithLetters_ReturnsInvalidTokenAndSyntaxError()
        {
            // arrange
            // act
            var result = this.tokeniser.Tokenise("12a3");

            // assert
            Assert_TokensIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_TokenHasValueAndPosition(result.First(), "12a3", 0);
        }

        [TestMethod]
        public void Tokenise_GivenTwoNumbersWithLetters_ReturnsTwoInvalidTokens()
        {
            // arrange
            // act
            var result = this.tokeniser.Tokenise("12a3 4b56");

            // assert
            Assert_TokensIsNotNullAndHasCount(result, 2);
            AssertCommon.Assert_TokenHasValueAndPosition(result.First(), "12a3", 0);
            AssertCommon.Assert_TokenHasValueAndPosition(result.Last(), "4b56", 5);
        }

        [TestMethod]
        public void Tokenise_GivenMixedInput_ReturnsCorrectTokens()
        {
            // arrange
            // act
            var result = this.tokeniser.Tokenise("word 12 12a3 word2 333");

            // assert
            Assert_TokensIsNotNullAndHasCount(result, 5);
            AssertCommon.Assert_TokenHasValueAndPosition(result.ElementAt(0), "word", 0);
            AssertCommon.Assert_TokenHasValueAndPosition(result.ElementAt(1), "12", 5);
            AssertCommon.Assert_TokenHasValueAndPosition(result.ElementAt(2), "12a3", 8);
            AssertCommon.Assert_TokenHasValueAndPosition(result.ElementAt(3), "word2", 13);
            AssertCommon.Assert_TokenHasValueAndPosition(result.ElementAt(4), "333", 19);
        }
    }
}
