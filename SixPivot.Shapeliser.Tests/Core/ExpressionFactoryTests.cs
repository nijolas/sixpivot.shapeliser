﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixPivot.Shapeliser.Core;
using System;
using System.Collections.Generic;

namespace SixPivot.Shapeliser.Tests.Core
{
    [TestClass]
    [TestCategory("Unit tests")]
    public class ExpressionFactoryTests
    {
        private ExpressionFactory expressionFactory;

        [TestInitialize]
        public void Initialise()
        {
            expressionFactory = new ExpressionFactory();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpressionAlreadyRegisteredException))]
        public void RegisterExpression_ExpressionAlreadyExists_Throws()
        {
            // arrange
            expressionFactory.RegisterExpression("test", (expressionFactory) => new TestExpression());

            // act, assert
            expressionFactory.RegisterExpression("test", (expressionFactory) => new TestExpression());
        }

        [TestMethod]
        [ExpectedException(typeof(NoExpressionsRegisteredException))]
        public void BuildExpression_NoExpressionsRegistered_Throws()
        {
            // arrange
            // do not register any expressions

            // act, assert
            // try to build one anyway
            expressionFactory.BuildExpression("test");
        }

        [TestMethod]
        [ExpectedException(typeof(ExpressionNotRegisteredException))]
        public void BuildExpression_ExpressionNotRegistered_Throws()
        {
            // arrange
            expressionFactory.RegisterExpression("test", (expressionFactory) => new TestExpression());

            // act, assert
            expressionFactory.BuildExpression("test2");
        }

        [TestMethod]
        public void BuildExpression_ExpressionRegistered_ReturnsExpressionInstance()
        {
            // arrange
            expressionFactory.RegisterExpression("test", (expressionFactory) => new TestExpression());

            // act
            var expression = expressionFactory.BuildExpression("test");

            // assert
            Assert.IsNotNull(expression, "Expected the expression to be non-null.");
            Assert.IsInstanceOfType(expression, typeof(TestExpression), $"Expected the expression to be a TestExpression, got {expression.GetType()}.");
        }

        class TestExpression : IExpression
        {
            public ParsingResult Parse(IEnumerator<Token> tokenEnumerator)
            {
                // should never get called
                throw new NotImplementedException();
            }
        }
    }
}
