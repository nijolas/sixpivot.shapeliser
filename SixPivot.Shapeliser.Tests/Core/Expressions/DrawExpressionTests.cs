﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixPivot.Shapeliser.Core;
using SixPivot.Shapeliser.Core.Expressions;
using System.Collections.Generic;
using System.Linq;

namespace SixPivot.Shapeliser.Tests.Core.Expressions
{
    [TestClass]
    [TestCategory("Unit tests")]
    public class DrawExpressionExpressionTests
    {
        private IExpressionFactory expressionFactory;

        private IExpression drawExpression;

        [TestInitialize]
        public void Initialise()
        {
            expressionFactory = new ExpressionFactory();

            drawExpression = new DrawExpression(expressionFactory);
        }

        [TestMethod]
        [ExpectedException(typeof(NothingToParseException))]
        public void Parse_GivenNullTokens_Throws()
        {
            // arrange, act, assert
            drawExpression.Parse(null);
        }

        [TestMethod]
        [ExpectedException(typeof(NothingToParseException))]
        public void Parse_GivenEmptyTokens_Throws()
        {
            // arrange, act, assert
            drawExpression.Parse(new Token[0].AsEnumerable().GetEnumerator());
        }

        [TestMethod]
        public void Parse_GivenSuccessfulShape_ReturnsShapeParseResult()
        {
            // arrange
            var tokens = new[]
            {
                new Token("draw", 0),
                new Token("a", 5),
                new Token("circle", 7),
                new Token("with", 14)
            };

            expressionFactory.RegisterExpression("shape", (expressionFactory) => new SuccessfulShapeMock());

            // act
            var result = drawExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 0);
        }

        [TestMethod]
        public void Parse_GivenFailingShape_ReturnsSyntaxError()
        {
            // arrange
            var tokens = new[]
            {
                new Token("draw", 0),
                new Token("a", 5),
                new Token("circle", 7),
                new Token("with", 12)
            };

            expressionFactory.RegisterExpression("shape", (expressionFactory) => new FailingShapeMock());

            // act
            var result = drawExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result, "Expected the parsing result to be non-null.");
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "error message", 1, ErrorLevel.Error);
        }

        /// <summary>
        /// This is a hand-made mock of a succesfully parsing shape.
        /// 
        /// Ideally this could be replaced with a mocking framework
        /// such as Chet, RhinoMocks or Moq.
        /// </summary>
        class SuccessfulShapeMock : IExpression, IMockExpressionWithTokenEnumerator
        {
            private IEnumerator<Token> tokenEnumerator;

            public ParsingResult Parse(IEnumerator<Token> tokenEnumerator)
            {
                this.tokenEnumerator = tokenEnumerator;
                return new ParsingResult(this, new SyntaxError[0]);
            }

            public IEnumerator<Token> TokenEnumerator => tokenEnumerator;
        }

        /// <summary>
        /// This is a hand-made mock of a shape that fails to parse.
        /// 
        /// Ideally this could be replaced with a mocking framework
        /// such as Chet, RhinoMocks or Moq.
        /// </summary>
        class FailingShapeMock : IExpression
        {
            public ParsingResult Parse(IEnumerator<Token> tokenEnumerator)
            {
                return new ParsingResult(null, new[] { new SyntaxError("error message", 1) });
            }
        }
    }
}
