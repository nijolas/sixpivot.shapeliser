﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixPivot.Shapeliser.Core;
using SixPivot.Shapeliser.Core.Expressions;
using System.Collections.Generic;
using System.Linq;

namespace SixPivot.Shapeliser.Tests.Core.Expressions
{
    [TestClass]
    [TestCategory("Unit tests")]
    public class DimensionExpressionTests
    {
        private IExpression dimensionExpression;

        [TestInitialize]
        public void Initialise()
        {
            dimensionExpression = new DimensionExpression();
        }

        void Assert_DimensionExpressionIsNotNullAndHasTypeAndValue(DimensionExpression expression, string type, int typePosition, string value, int valuePosition)
        {
            Assert.IsNotNull(expression, "Expected DimensionExpression to be non-null.");
            AssertCommon.Assert_TokenHasValueAndPosition(expression.DimensionType, type, typePosition);
            AssertCommon.Assert_TokenHasValueAndPosition(expression.DimensionValue, value, valuePosition);
        }

        [TestMethod]
        [ExpectedException(typeof(NothingToParseException))]
        public void Parse_GivenNullTokens_Throws()
        {
            // arrange, act, assert
            dimensionExpression.Parse(null);
        }

        [TestMethod]
        public void Parse_GivenEmptyTokens_ReturnsSyntaxError()
        {
            // arrange, act
            var result = dimensionExpression.Parse(new Token[0].AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "dimension", SyntaxError.EOF, ErrorLevel.Error);
        }

        [TestMethod]
        public void Parse_GivenMinimumSyntax_ReturnsCorrectResult()
        {
            // arrange
            var tokens = new[]
            {
                new Token("radius", 0),
                new Token("6", 1),
            };

            // arrange, act
            var result = dimensionExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 0);
            Assert_DimensionExpressionIsNotNullAndHasTypeAndValue(result.Expression as DimensionExpression, "radius", 0, "6", 1);
        }

        [TestMethod]
        public void Parse_GivenMaximumSyntax_ReturnsCorrectResult()
        {
            // arrange
            var tokens = new[]
            {
                new Token("with", 0),
                new Token("a", 1),
                new Token("radius", 2),
                new Token("of", 3),
                new Token("6", 4),
            };

            // arrange, act
            var result = dimensionExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 0);
            Assert_DimensionExpressionIsNotNullAndHasTypeAndValue(result.Expression as DimensionExpression, "radius", 2, "6", 4);
        }

        [TestMethod]
        public void Parse_GivenJustWith_ReturnsEof()
        {
            // arrange
            var tokens = new[]
            {
                new Token("with", 0)
            };

            // arrange, act
            var result = dimensionExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "dimension", SyntaxError.EOF, ErrorLevel.Error);
        }

        [TestMethod]
        public void Parse_GivenJustA_ReturnsEof()
        {
            // arrange
            var tokens = new[]
            {
                new Token("a", 1)
            };

            // arrange, act
            var result = dimensionExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "dimension", SyntaxError.EOF, ErrorLevel.Error);
        }

        [TestMethod]
        public void Parse_GivenJustJoiningWords_ReturnsEof()
        {
            // arrange
            var tokens = new[]
            {
                new Token("with", 1),
                new Token("a", 2),
                new Token("of", 3)
            };

            // arrange, act
            var result = dimensionExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "dimension", SyntaxError.EOF, ErrorLevel.Error);
        }
    }
}
