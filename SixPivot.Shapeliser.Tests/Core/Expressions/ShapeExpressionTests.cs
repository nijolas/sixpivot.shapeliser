﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixPivot.Shapeliser.Core;
using SixPivot.Shapeliser.Core.Expressions;
using System.Collections.Generic;
using System.Linq;

namespace SixPivot.Shapeliser.Tests.Core.Expressions
{
    [TestClass]
    [TestCategory("Unit tests")]
    public class ShapeExpressionTests
    {
        private IExpressionFactory expressionFactory;

        private IExpression shapeExpression;

        [TestInitialize]
        public void Initialise()
        {
            expressionFactory = new ExpressionFactory();

            shapeExpression = new ShapeExpression(expressionFactory);
        }

        [TestMethod]
        [ExpectedException(typeof(NothingToParseException))]
        public void Parse_GivenNullTokens_Throws()
        {
            // arrange, act, assert
            shapeExpression.Parse(null);
        }

        [TestMethod]
        public void Parse_GivenEmptyTokens_ReturnsSyntaxError()
        {
            // arrange, act
            var result = shapeExpression.Parse(new Token[0].AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "shape", SyntaxError.EOF, ErrorLevel.Error);
        }

        [TestMethod]
        public void Parse_MissingIndefiniteArticle_Warns()
        {
            // arrange
            var tokens = new[]
            {
                new Token("circle", 5),
                new Token("with", 10),
                new Token("radius", 15)
            };

            expressionFactory.RegisterExpression("dimension", (expressionFactory) => new SuccessfulDimensionMock());

            // act
            var result = shapeExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "indefinite article", 5, ErrorLevel.Warning);
        }

        [TestMethod]
        public void Parse_GiveSuccessfulDimension_ReturnsDimensionParseResult()
        {
            // arange
            expressionFactory.RegisterExpression("dimension", (expressionFactory) => new SuccessfulDimensionMock());

            var tokens = new[]
            {
                new Token("a", 0),
                new Token("circle", 1),
                new Token("with", 2),
                new Token("radius", 3),
                new Token("6", 4),
            };

            // act
            var result = shapeExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 0);
        }

        [TestMethod]
        public void Parse_GiveFailingDimension_ReturnsSyntaxError()
        {
            // arange
            expressionFactory.RegisterExpression("dimension", (expressionFactory) => new FailingDimensionMock());

            var tokens = new[]
            {
                new Token("a", 1),
                new Token("circle", 2)
            };

            // act
            var result = shapeExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result, "Expected the parsing result to be non-null.");
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "error message", 1, ErrorLevel.Error);
        }

        [TestMethod]
        public void Parse_CombinesSyntaxErrors()
        {
            // arrange
            var tokens = new[]
            {
                new Token("circle", 5)
            };

            expressionFactory.RegisterExpression("dimension", (expressionFactory) => new FailingDimensionMock());

            // act
            var result = shapeExpression.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 2);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "indefinite article", 5, ErrorLevel.Warning);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.Last(), "error message", 1, ErrorLevel.Error);
        }

        /// <summary>
        /// This is a hand-made mock of a succesfully parsing dimension.
        /// 
        /// Ideally this could be replaced with a mocking framework
        /// such as Chet, RhinoMocks or Moq.
        /// </summary>
        class SuccessfulDimensionMock : IExpression, IMockExpressionWithTokenEnumerator
        {
            private IEnumerator<Token> tokenEnumerator;

            public ParsingResult Parse(IEnumerator<Token> tokenEnumerator)
            {
                this.tokenEnumerator = tokenEnumerator;
                return new ParsingResult(this, new SyntaxError[0]);
            }

            public IEnumerator<Token> TokenEnumerator => tokenEnumerator;
        }

        /// <summary>
        /// This is a hand-made mock of a dimension that fails to parse.
        /// 
        /// Ideally this could be replaced with a mocking framework
        /// such as Chet, RhinoMocks or Moq.
        /// </summary>
        class FailingDimensionMock : IExpression
        {
            public ParsingResult Parse(IEnumerator<Token> tokenEnumerator)
            {
                return new ParsingResult(null, new[] { new SyntaxError("error message", 1) });
            }
        }
    }
}
