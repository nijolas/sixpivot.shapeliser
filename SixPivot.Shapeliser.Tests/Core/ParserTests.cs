﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixPivot.Shapeliser.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixPivot.Shapeliser.Tests.Core
{
    [TestClass]
    [TestCategory("Unit tests")]
    public class ParserTests
    {
        private IExpressionFactory expressionFactory;

        private Parser parser;

        [TestInitialize]
        public void Intialise()
        {
            expressionFactory = new ExpressionFactory();

            this.parser = new Parser(expressionFactory);
        }

        [TestMethod]
        [ExpectedException(typeof(NothingToParseException))]
        public void Parse_GivenNullInput_ShouldThrowException()
        {
            // arrange
            // act, assert
            var result = parser.Parse(null);
        }

        [TestMethod]
        [ExpectedException(typeof(NothingToParseException))]
        public void Parse_GivenEmptyInput_ShouldThrowException()
        {
            // arrange
            // act, assert
            var result = parser.Parse(new Token[0].AsEnumerable().GetEnumerator());
        }

        [TestMethod]
        public void Parse_CommandIsNotFound_ShouldReturnSyntaxError()
        {
            // arrange
            expressionFactory.RegisterExpression("draw", (expressionFactory) => new SuccessfulCommandMock());

            var tokens = new List<Token>
            {
                new Token("invalid", 0)
            };

            // act
            var result = parser.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "invalid", 0, ErrorLevel.Error);
        }

        [TestMethod]
        public void Parse_CommandIsFound_ShouldCallParseAndReturnSuccessfulResult()
        {
            // arrange
            expressionFactory.RegisterExpression("draw", (expressionFactory) => new SuccessfulCommandMock());

            var tokens = new List<Token>
            {
                new Token("draw", 0),
                new Token("a", 5)
            };

            // act
            var result = parser.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result, "Expected the parsing result to be non-null.");
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 0);
            AssertCommon.Assert_ExpressionIsTypeAndNextTokenHasValueAndPosition<SuccessfulCommandMock>(result.Expression, "draw", 0);
        }

        [TestMethod]
        public void Parse_CommandIsFound_ShouldCallParseAndReturnFailingResult()
        {
            // arrange
            expressionFactory.RegisterExpression("draw", (expressionFactory) => new FailingCommandMock());

            var tokens = new List<Token>
            {
                new Token("draw", 0)
            };

            // act
            var result = parser.Parse(tokens.AsEnumerable().GetEnumerator());

            // assert
            Assert.IsNotNull(result, "Expected the parsing result to be non-null.");
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, 1);
            AssertCommon.Assert_SyntaxErrorContainsTextHasPositionAndLevel(result.Errors.First(), "error message", 1, ErrorLevel.Error);
        }

        /// <summary>
        /// This is a hand-made mock of a succesfully parsing command.
        /// 
        /// Ideally this could be replaced with a mocking framework
        /// such as Chet, RhinoMocks or Moq.
        /// </summary>
        class SuccessfulCommandMock : IExpression, IMockExpressionWithTokenEnumerator
        {
            private IEnumerator<Token> tokenEnumerator;

            public ParsingResult Parse(IEnumerator<Token> tokenEnumerator)
            {
                this.tokenEnumerator = tokenEnumerator;
                return new ParsingResult(this, new SyntaxError[0]);
            }

            public IEnumerator<Token> TokenEnumerator => tokenEnumerator;
        }

        /// <summary>
        /// This is a hand-made mock of a command that fails to parse.
        /// 
        /// Ideally this could be replaced with a mocking framework
        /// such as Chet, RhinoMocks or Moq.
        /// </summary>
        class FailingCommandMock : IExpression
        {
            public ParsingResult Parse(IEnumerator<Token> tokenEnumerator)
            {
                return new ParsingResult(null, new[] { new SyntaxError("error message", 1) });
            }
        }
    }
}
