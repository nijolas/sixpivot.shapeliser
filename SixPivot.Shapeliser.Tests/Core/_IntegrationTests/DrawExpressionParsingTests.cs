﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixPivot.Shapeliser.Core;
using SixPivot.Shapeliser.Core.Expressions;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SixPivot.Shapeliser.Tests.Core._IntegrationTests
{
    [TestClass]
    [TestCategory("Integration tests")]
    public class DrawExpressionParsingTests
    {
        private ExpressionFactory expressionFactory;

        private Tokeniser tokeniser;
        private Parser parser;

        [TestInitialize]
        public void Initialise()
        {
            expressionFactory = new ExpressionFactory();

            expressionFactory.RegisterExpression("draw", (expressionFactory) => new DrawExpression(expressionFactory));
            expressionFactory.RegisterExpression("shape", (expressionFactory) => new ShapeExpression(expressionFactory));
            expressionFactory.RegisterExpression("dimension", (expressionFactory) => new DimensionExpression());

            tokeniser = new Tokeniser();
            parser = new Parser(expressionFactory);
        }

        void Assert_ResultNotNullAndHasErrorCount(ParsingResult result, int count)
        {
            Assert.IsNotNull(result);
            AssertCommon.Assert_ParsingResultErrorsIsNotNullAndHasCount(result, count);
        }

        List<DimensionExpression> Assert_DrawAndShapeExpressionsCorrect_ReturnsDimensionExpressions(ParsingResult result, string shapeName, int shapePosition, int numberOfDimensions)
        {
            var drawExpression = result.Expression as DrawExpression;
            Assert.IsNotNull(drawExpression, $"Expected a DrawExpression, got {drawExpression.GetType().Name}");

            var shapeExpression = drawExpression.ShapeExpression;
            Assert.IsNotNull(shapeExpression, "Expected ShapeExpression to be non-null.");
            AssertCommon.Assert_TokenHasValueAndPosition(shapeExpression.ShapeToken, shapeName, shapePosition);

            var dimensionExpressions = shapeExpression.DimensionExpressions;
            Assert.IsNotNull(dimensionExpressions, "Expected DimensionExpressions to be non-null.");
            Assert.AreEqual(numberOfDimensions, dimensionExpressions.Count, $"Expected DimensionExpressions to have count {numberOfDimensions}, got {dimensionExpressions.Count}.");

            return dimensionExpressions;
        }

        [TestMethod]
        public void DrawACircleWithRadiusSix_ReturnsCorrectResult()
        {
            // arrange
            var commandInput = "draw a circle with radius 6";

            // act
            var tokens = tokeniser.Tokenise(commandInput);
            var result = parser.Parse(tokens.GetEnumerator());

            // assert
            Assert_ResultNotNullAndHasErrorCount(result, 0);
            var dimensionExpressions = Assert_DrawAndShapeExpressionsCorrect_ReturnsDimensionExpressions(result, "circle", 7, 1);

            var radiusDimensionExpression = dimensionExpressions.First();
            AssertCommon.Assert_TokenHasValueAndPosition(radiusDimensionExpression.DimensionType, "radius", 19);
            AssertCommon.Assert_TokenHasValueAndPosition(radiusDimensionExpression.DimensionValue, "6", 26);
        }

        [TestMethod]
        public void DrawCircleRadiusTwenty_ReturnsCorrectResult()
        {
            // arrange
            var commandInput = "draw circle radius 20";

            // act
            var tokens = tokeniser.Tokenise(commandInput);
            var result = parser.Parse(tokens.GetEnumerator());

            // assert
            Assert_ResultNotNullAndHasErrorCount(result, 1);
            Assert.IsFalse(result.Errors.Any(e => e.Level == ErrorLevel.Error), "Only expected warnings, but got some errors.");
            var dimensionExpressions = Assert_DrawAndShapeExpressionsCorrect_ReturnsDimensionExpressions(result, "circle", 5, 1);

            var radiusDimensionExpression = dimensionExpressions.First();
            AssertCommon.Assert_TokenHasValueAndPosition(radiusDimensionExpression.DimensionType, "radius", 12);
            AssertCommon.Assert_TokenHasValueAndPosition(radiusDimensionExpression.DimensionValue, "20", 19);
        }

        [TestMethod]
        public void DrawARectangleWithAWidthOfFiveAndHeightTen_ReturnsCorrectResult()
        {
            // arrange
            var commandInput = "draw a rectangle with a width of 5 and height 10";

            // act
            var tokens = tokeniser.Tokenise(commandInput);
            var result = parser.Parse(tokens.GetEnumerator());

            // assert
            Assert_ResultNotNullAndHasErrorCount(result, 0);
            var dimensionExpressions = Assert_DrawAndShapeExpressionsCorrect_ReturnsDimensionExpressions(result, "rectangle", 7, 2);

            var widthDimensionExpression = dimensionExpressions.First();
            AssertCommon.Assert_TokenHasValueAndPosition(widthDimensionExpression.DimensionType, "width", 24);
            AssertCommon.Assert_TokenHasValueAndPosition(widthDimensionExpression.DimensionValue, "5", 33);

            var heightDimensionExpression = dimensionExpressions.Last();
            AssertCommon.Assert_TokenHasValueAndPosition(heightDimensionExpression.DimensionType, "height", 39);
            AssertCommon.Assert_TokenHasValueAndPosition(heightDimensionExpression.DimensionValue, "10", 46);
        }

        [TestMethod]
        public void DrawAnOctagonWithRadiusOfFive_ReturnsCorrectResult()
        {
            // arrange
            var commandInput = "draw an octagon with radius of 5";

            // act
            var tokens = tokeniser.Tokenise(commandInput);
            var result = parser.Parse(tokens.GetEnumerator());

            // assert
            Assert_ResultNotNullAndHasErrorCount(result, 0);
            var dimensionExpressions = Assert_DrawAndShapeExpressionsCorrect_ReturnsDimensionExpressions(result, "octagon", 8, 1);

            var radiusDimensionExpression = dimensionExpressions.First();
            AssertCommon.Assert_TokenHasValueAndPosition(radiusDimensionExpression.DimensionType, "radius", 21);
            AssertCommon.Assert_TokenHasValueAndPosition(radiusDimensionExpression.DimensionValue, "5", 31);
        }

        [TestMethod]
        public void DrawATriangleWithBaseTenAndHeightSeven_ReturnsCorrectResult()
        {
            // arrange
            var commandInput = "draw a triangle with base 10 and height 7";

            // act
            var tokens = tokeniser.Tokenise(commandInput);
            var result = parser.Parse(tokens.GetEnumerator());

            // assert
            Assert_ResultNotNullAndHasErrorCount(result, 0);
            var dimensionExpressions = Assert_DrawAndShapeExpressionsCorrect_ReturnsDimensionExpressions(result, "triangle", 7, 2);

            var widthDimensionExpression = dimensionExpressions.First();
            AssertCommon.Assert_TokenHasValueAndPosition(widthDimensionExpression.DimensionType, "base", 21);
            AssertCommon.Assert_TokenHasValueAndPosition(widthDimensionExpression.DimensionValue, "10", 26);

            var heightDimensionExpression = dimensionExpressions.Last();
            AssertCommon.Assert_TokenHasValueAndPosition(heightDimensionExpression.DimensionType, "height", 33);
            AssertCommon.Assert_TokenHasValueAndPosition(heightDimensionExpression.DimensionValue, "7", 40);
        }
    }
}
