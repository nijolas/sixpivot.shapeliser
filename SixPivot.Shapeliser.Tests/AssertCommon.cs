﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixPivot.Shapeliser.Core;
using System.Collections.Generic;
using System.Linq;

namespace SixPivot.Shapeliser.Tests
{
    internal static class AssertCommon
    {
        public static void Assert_ParsingResultErrorsIsNotNullAndHasCount(ParsingResult result, int count)
        {
            Assert.IsNotNull(result.Errors, "Expected Errors to be non-null.");
            Assert.AreEqual(count, result.Errors.Count(), $"Expected there to be {count} errors, but found {result.Errors.Count()}.");
        }

        public static void Assert_SyntaxErrorContainsTextHasPositionAndLevel(SyntaxError error, string searchText, int position, ErrorLevel level)
        {
            Assert.IsNotNull(error, "Expected error to be non-null.");
            Assert.IsTrue(error.Message.Contains(searchText), $"Expected error to contain text '{searchText}`.");
            Assert.AreEqual(position, error.Position, $"Expected error to have position {position}, but got {error.Position}.");
            Assert.AreEqual(level, error.Level, $"Expected error to have level {level}, but got {error.Level}.");
        }

        public static void Assert_TokenHasValueAndPosition(Token token, string value, int position)
        {
            Assert.IsNotNull(token, "Expected a token but got null.");
            Assert.AreEqual(value, token.Value, $"Expected Token with value \"{value}\", but got \"{token.Value}\".");
            Assert.AreEqual(position, token.Position, $"Expected Token to have position {position}, but got {token.Position}.");
        }

        public static void Assert_ExpressionIsTypeAndNextTokenHasValueAndPosition<TType>(IExpression expression, string tokenValue, int tokenPosition)
            where TType : class, IMockExpressionWithTokenEnumerator
        {
            var typed = expression as TType;
            Assert.IsNotNull(typed, $"Expected expression to be of type {typeof(TType).Name}, got {expression.GetType().Name}.");

            if (!typed.TokenEnumerator.MoveNext()) Assert.Fail($"Expected the enumerator to still have tokens.");
            Assert_TokenHasValueAndPosition(typed.TokenEnumerator.Current, tokenValue, tokenPosition);
        }
    }

    public interface IMockExpressionWithTokenEnumerator
    {
        IEnumerator<Token> TokenEnumerator { get; }
    }
}
