﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SixPivot.Shapeliser.Tests
{
    public class MockServiceProvider : IServiceProvider
    {
        public object GetService(Type serviceType)
        {
            return Activator.CreateInstance(serviceType);
        }
    }
}
