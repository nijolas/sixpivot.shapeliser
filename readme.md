﻿# SixPivot.Shapeliser

Shapeliser is a browser-based tool for generating shapes based on semi-natural language input.

## Getting Started

To build the project from source, simply:

1. Clone the repository
```
git clone https://bitbucket.org/nijolas/sixpivot.shapeliser.git
```
1. Open SixPivot.Shapeliser.sln in [Visual Studio 2017](https://www.visualstudio.com/downloads/)
1. Build and Run the project (press F5)
1. If your browser does not open automatically, open it and browse to [http://localhost:62439/]

To test everything's working, type a simple command into the prompt, and click the "DRAW" button.

```
draw a circle with a radius of 100
```

### Prerequisites

For everything to go smoothly, first ensure you have the following tools installed:

* [Visual Studio 2017](https://www.visualstudio.com/downloads/)
* [Nuget](https://docs.microsoft.com/en-us/nuget/guides/install-nuget#nuget-package-manager-in-visual-studio) (included with VS 2017)
* [git](https://git-scm.com/downloads) (included with VS 2017)
* [NPM](https://www.npmjs.com/get-npm) (included with VS 2017)

### Running the tests

Shapeliser uses the [Microsoft Unit Test Framework](https://msdn.microsoft.com/en-us/library/hh598960.aspx) which is intimately integrated with Visual Studio.

To run the tests, press the key combination (Ctrl+R, A), or alternately run them via the menu:

```
Test -> Run -> All Tests
```

## Design

### Assumptions and considerations

Shapeliser is designed to be usable by a junior C#/React programmer with no handover and no outside assistance:

```
As a junior developer, I want to check out, build, test and deploy Shapeliser without assistance.
```

Shapeliser is designed to be extensible. Given the project scope, this extensibility has been narrowly defined:

```
As a junior developer, I want to be able to add a new shape definition without assistance.
```

This has lead to the following design decisions:

* No external libraries outside standard Microsoft includes and React itself
* No interdependency in SixPivot.Shapeliser.Core to avoid DI/IoC and mocking
* A simple tokeniser and a basic, but extensible, parser [^1]
* Plenty of documentation that assumes no knowledge of conventions or idiomatic C#
* But also **sticking** to conventions and idiomatic C# as much as possible
* Azure deployment strategy

Given this, the most difficult concepts in Shapeliser should be parsing and geometry. And the only assumed knowledge of future maintainers is baseline C#/React competency.

[^1]: As opposed to this step being obfuscated by a tool, like ANTLR, requiring extra knowledge. This has the side-benefit of simpler handling of the weak grammar Shapeliser supports.

### Project layout

* SixPivot.Shapeliser.Core - the core parsing and shape generating functionality
* SixPivot.Shapeliser.Web - the ASP.NET Core Web API and ReactJS front-end
* SixPivot.Shapeliser.Tests - the test project

### Style

This project follows the [C# Coding Conventions](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/coding-conventions) laid out by Microsoft.

### Testing

Shapeliser has been designed using the [TDD](https://martinfowler.com/bliki/TestDrivenDevelopment.html) approach known as ["Red Green Refactor"](http://www.jamesshore.com/Blog/Red-Green-Refactor.html).

All unit tests are (and should continue to be) written in [AAA](http://wiki.c2.com/?ArrangeActAssert) style.

For more details, see [testing.md](testing.md).

## Deployment

To deploy Shapeliser, right-click on the project `SixPivot.Shapeliser.Web` in Visual Studio and click Publish.

You will need appropriate Azure credentials.

## Built With

* [ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/) - The web framework used
* [ReactJS](https://reactjs.org/) - Front-end UI framework

## Versioning

Shapeliser uses [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Contributing

The Shapeliser repository lives and dies on [good commit messages](https://chris.beams.io/posts/git-commit/). Bone up!

## Authors

* **Nicholas Wilson** - [nijolas](https://bitbucket.org/nijolas)

## License

This project is licensed under the MIT License - see the [license.md](license.md) file for details

## Acknowledgments

* Microsoft for keeping up with the times
* SixPivot for such a cool puzzle
* Mom's spaghetti
