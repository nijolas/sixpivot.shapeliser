﻿var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        main: './index.js'
    },
    output: {
        path: path.resolve(__dirname, 'wwwroot'),
        filename: 'shapeliser.js',
        publicPath: '/'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'env'],
                    plugins: ['transform-object-rest-spread','transform-class-properties']
                }
            }
        ]
    },
    stats: {
        colors: true
    },
    devtool: 'source-map'
};