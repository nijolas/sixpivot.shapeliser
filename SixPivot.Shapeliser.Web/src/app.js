﻿import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import Heading from './components/heading'
import ShapeDrawer from './components/shapeDrawer'
import CommandInput from './components/commandInput'

// ensure our prototypes get set up
import Prototypes from './prototypes'

class Shapeliser extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            shape: null,
            command: null
        }
    }

    handleReceiveCommand({ expression }) {
        this.setState({
            shape: expression.shapeExpression,
            command: expression
        })
    }

    render() {
        let { shape, command } = this.state

        return (
            <div id="shapeliser">
                <Heading />
                <ShapeDrawer shape={shape} />
                <CommandInput command={command} onReceiveCommand={this.handleReceiveCommand.bind(this)} />
            </div>
        )
    }
}

ReactDOM.render(
    <Shapeliser />,
    document.getElementById("react-root")
)