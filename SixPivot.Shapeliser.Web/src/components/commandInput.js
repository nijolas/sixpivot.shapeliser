import React, { Component } from 'react'
import axios from 'axios'

export default class CommandInput extends Component {
    handleGo() {
        let { onReceiveCommand } = this.props
        var command = this.refs.command.value

        if (!command) {
            alert('Please type something in before clicking Go!')
        } else {
            axios
                .get(`/api/execute?command=${command}`)
                .then(({ data }) => {
                    onReceiveCommand(data)
                })
        }
    }

    render() {
        return (
            <div className="command-input-container">
                <input ref="command" type="text" defaultValue="draw a circle with radius 120" placeholder="draw a circle with radius 120" />
                <button onClick={this.handleGo.bind(this)}>Go!</button>
            </div>
        )
    }
}