import React, { Component } from 'react'

import { nextColour } from '../colours'

export default class Heading extends Component {
    shouldComponentUpdate(nextProps) {
        return false
    }

    render() {
        return (
            <h1>
                <span style={{ color: nextColour() }}>S</span>
                <span style={{ color: nextColour() }}>h</span>
                <span style={{ color: nextColour() }}>a</span>
                <span style={{ color: nextColour() }}>p</span>
                <span style={{ color: nextColour() }}>e</span>
                <span style={{ color: nextColour() }}>l</span>
                <span style={{ color: nextColour() }}>i</span>
                <span style={{ color: nextColour() }}>s</span>
                <span style={{ color: nextColour() }}>e</span>
                <span style={{ color: nextColour() }}>r</span>
            </h1>
        )
    }
}