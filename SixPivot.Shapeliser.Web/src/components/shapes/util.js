export const validateDimensions = (expected, actual) => {
    let actualCopy = [ ...actual ]
    let dimensions = { }

    for (var i = 0; i < expected.length; i++) {
        let expectedDimensionType = expected[i]

        // search for a matching dimension
        let matchingDimension = actualCopy.firstOrDefault(d => d.dimensionType.value === expectedDimensionType)

        if (matchingDimension) {
            // add it to the result
            var dimensionValue = Number(matchingDimension.dimensionValue.value)
            if (isNaN(dimensionValue)) {
                return false
            } else {
                dimensions[matchingDimension.dimensionType.value] = dimensionValue
            }

            // remove it from the search list
            actualCopy.splice(i, 1)
        }
        else {
            // otherwise we don't have the correct dimensions
            return false
        } 
    }

    return dimensions
}