import { validateDimensions } from './util'

const drawTriangleFromBaseAndHeight = (context, x, y, base, height, colour) => {
    // we have a center, so let's figure out the rectangle in which
    // to draw this triangle
    let left = x - base / 2
    let right = x + base / 2
    let bottom = y + height / 2
    let top = y - height / 2

    context.beginPath()
    // start at the bottom left
    context.moveTo(left, bottom)
    // line across the base
    context.lineTo(right, bottom)
    // line to the top
    context.lineTo(x, top)
    // draw
    context.fillStyle = colour
    context.fill()
}

export default (context, shape, position, colour) => {
    let { x, y } = position
    let base, height

    // look for base and height
    let dimensions = validateDimensions([ "base", "height" ], shape.dimensionExpressions)
    if (dimensions) {
        base = dimensions.base
        height = dimensions.base
    }
    
    // look for side
    dimensions = validateDimensions([ "side" ], shape.dimensionExpressions)
    if (dimensions) {
        base = dimensions.side
        // calculate the height of an equaliateral triangle from its side
        height = (base * Math.pow(3, 0.5)) / 2
    }

    if (base && height) {
        drawTriangleFromBaseAndHeight(context, x, y, base, height, colour)
    } else {
        alert("Triangles require two dimensions 'base' and 'height' - or a single dimension 'side'.")
    }
}