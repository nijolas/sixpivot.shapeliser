import { validateDimensions } from './util'

export default (context, shape, position, colour) => {
    let { x, y } = position
    let width, height

    // look for width and height
    let dimensions = validateDimensions([ "width", "height" ], shape.dimensionExpressions)
    if (dimensions) {
        width = dimensions.width
        height = dimensions.height
    }

    if (width && height) {
        context.beginPath()
        context.ellipse(x, y, width, height, 0, Math.PI * 2, 0)
        context.fillStyle = colour
        context.fill()
    } else {
        alert("Ellipses require two dimensions 'width' and 'height'.")
    }
}