import { validateDimensions } from './util'

export default (context, shape, position, colour) => {
    let { x, y } = position
    let side

    // look for side
    let dimensions = validateDimensions([ "side" ], shape.dimensionExpressions)
    if (dimensions) {
        side = dimensions.side
    }

    // look for perimeter
    dimensions = validateDimensions([ "perimeter" ], shape.dimensionExpressions)
    if (dimensions) {
        side = dimensions.perimeter / 4
    }

    // look for area
    dimensions = validateDimensions([ "area" ], shape.dimensionExpressions)
    if (dimensions) {
        side = Math.pow(dimensions.area, 0.5)
    }

    if (side) {
        let left = x - side / 2
        let top = y - side / 2

        context.fillStyle = colour
        context.fillRect(left, top, side, side)
    } else {
        alert("Squares require a single dimension of either 'side', 'perimeter' or 'area'.")
    }
}