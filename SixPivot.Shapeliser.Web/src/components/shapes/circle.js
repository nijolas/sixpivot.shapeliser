import { validateDimensions } from './util'

export default (context, shape, position, colour) => {
    let dimensions = validateDimensions([ "radius" ], shape.dimensionExpressions)
    if (dimensions) {
        let { x, y } = position
        let { radius } = dimensions

        context.beginPath()
        context.arc(x, y, radius, 0, Math.PI * 2)
        context.fillStyle = colour
        context.fill()
    } else {
        alert("Circles require a single dimension 'radius'.");
    }
}