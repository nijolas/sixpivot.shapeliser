import circle from './circle'
import triangle from './triangle'
import rectangle from './rectangle'
import square from './square'
import polygon from './polygon'
import ellipse from './ellipse'

const shapeHandlers = [
    {
        test: /^circle$/i,
        handle: circle
    },
    {
        test: /^triangle$/i,
        handle: triangle
    },
    {
        test: /^rectangle$/i,
        handle: rectangle
    },
    {
        test: /^square$/i,
        handle: square
    },
    {
        test: /gon$/i,
        handle: polygon
    },
    {
        test: /^(oval|ellipse)$/i,
        handle: ellipse
    }
]

export const renderShape = (context, shape, position, colour) => {
    let shapeName = shape.shapeToken.value

    let handler = shapeHandlers.firstOrDefault(h => shapeName.match(h.test))
    if (handler) {
        handler.handle(context, shape, position, colour)
    }
    else {
        alert(`Unrecognised shape '${shapeName}' at position ${shape.shapeToken.position}.`)
    }
}