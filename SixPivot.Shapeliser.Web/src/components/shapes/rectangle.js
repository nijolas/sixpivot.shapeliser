import { validateDimensions } from './util'

const drawRectangleFromWidthAndHeight = (context, x, y, width, height, colour) => {
    // we have a center, so let's figure out the
    // top left corner (where the canvas starts drawing from)
    let left = x - width / 2
    let top = y - height / 2

    // draw
    context.fillStyle = colour
    context.fillRect(left, top, width, height)
}

export default (context, shape, position, colour) => {
    let { x, y } = position
    let width, height

    // look for width and height
    let dimensions = validateDimensions([ "width", "height" ], shape.dimensionExpressions)
    if (dimensions) {
        width = dimensions.width
        height = dimensions.height
    }

    if (width && height) {
        drawRectangleFromWidthAndHeight(context, x, y, width, height, colour)
    } else {
        alert("Rectangles require two dimensions 'width' and 'height'.")
    }
}