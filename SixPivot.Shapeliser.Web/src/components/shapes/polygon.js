import { validateDimensions } from './util'

const drawPolygonFromRadiusAndSides = (context, x, y, radius, numberOfSides, colour) => {
    // we draw a regular polygon by joining
    // points found on a circle with a given radius
    let angleStep = (Math.PI * 2) / numberOfSides

    // the first point is at 3 o'clock
    context.moveTo(x + radius, y)

    for (var i = 1; i < numberOfSides; i++) {
        // find the point
        let pointX = x + radius * Math.cos(angleStep * i)
        let pointY = y + radius * Math.sin(angleStep * i)

        context.lineTo(pointX, pointY)
    }

    // the last point is at 3 o'clock
    context.lineTo(x + radius, y)

    context.fillStyle = colour
    context.fill()
}

const calculateRadiusFromSide = (numberOfSides, side) => {
    // source: https://www.mathopenref.com/polygonradius.html
    return side / (2 * Math.sin(Math.PI / numberOfSides))
}

const calculateRadiusFromArea = (numberOfSides, area) => {
    // source: https://www.mathopenref.com/polygonregulararea.html and some algebra
    let side = Math.pow((4 * area * Math.tan(Math.PI / numberOfSides)) / numberOfSides, 0.5)
    return calculateRadiusFromSide(numberOfSides, side)
}

export default (context, shape, position, colour) => {
    let { x, y } = position
    let radius

    // calculate the number of sides
    let numberOfSides = nameToSides(shape.shapeToken.value)
    if (!numberOfSides) {
        alert(`Did not recognise polygon with name '${shape.shapeToken.value}'.`)
        return
    }

    // look for radius
    let dimensions = validateDimensions([ "radius" ], shape.dimensionExpressions)
    if (dimensions) {
        radius = dimensions.radius
    }

    // look for side
    dimensions = validateDimensions([ "side" ], shape.dimensionExpressions)
    if (dimensions) {
        radius = calculateRadiusFromSide(numberOfSides, dimensions.side)
    }

    // look for perimeter
    dimensions = validateDimensions([ "perimeter" ], shape.dimensionExpressions)
    if (dimensions) {
        radius = calculateRadiusFromSide(numberOfSides, dimensions.perimeter / numberOfSides)
    }

    // look for area
    dimensions = validateDimensions([ "area" ], shape.dimensionExpressions)
    if (dimensions) {
        radius = calculateRadiusFromArea(numberOfSides, dimensions.area)
    }

    if (radius && numberOfSides) {
        drawPolygonFromRadiusAndSides(context, x, y, radius, numberOfSides, colour)
    } else {
        alert("Polygons require a single dimension of either 'radius', 'side', 'perimeter' or 'area'.")
    }
}

// source: https://en.wikipedia.org/wiki/List_of_polygons#List_of_n-gons_by_Greek_numerical_prefixes
const nameToSides = (name) => {
    switch (name) {
        case "trigon": return 3
        
        case "tetragon": return 4

        case "pentagon": return 5

        case "hexagon": return 6

        case "heptagon": return 7
        case "septagon": return 7

        case "octagon": return 8

        case "enneagon": return 9
        case "nonagon": return 9

        case "decagon": return 10

        case "hendecagon": return 11
        case "undecagon": return 11

        case "dodecagon": return 12

        // return falsy for "don't know"
        default: return 0
    }
}