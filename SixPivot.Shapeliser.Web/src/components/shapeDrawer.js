import React, { Component } from 'react'

import { renderShape } from './shapes'
import { nextColour } from '../colours'

export default class ShapeDrawer extends Component {
    componentDidMount() {
        this.update(this.props)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.update(nextProps)
    }

    update(props) {
        let { canvas } = this.refs
        let context = canvas.getContext('2d')

        let { clientWidth: width, clientHeight: height } = canvas

        // update width and height
        canvas.width = width
        canvas.height = height

        // clear the screen
        context.clearRect(0, 0, width, height)

        let { shape } = props
        if (shape) {
            renderShape(context, shape, { x: width / 2, y: height / 2 }, nextColour())
        }

        // ensure React doesn't re-render our canvas element and wreck everything
        return false
    }

    render() {
        return (
            <canvas id="canvas" width="100" height="100" ref="canvas">

            </canvas>
        )
    }
}