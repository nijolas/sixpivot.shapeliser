const colours = {
    PINK: "#CF2154",
    ORANGE: "#EE5F23",
    BLUE: "#6AD2EB",
    GREEN: "#9BD183",
    YELLOW: "#FFD908"
}

let numberOfColours = Object.keys(colours).length
let currentColour = Math.round(Math.random() * numberOfColours)

export const nextColour = () => {
    currentColour = (currentColour + 1) % numberOfColours
    return colours[Object.keys(colours)[currentColour]]
}