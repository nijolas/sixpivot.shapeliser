if (!Array.prototype.firstOrDefault) {
    Array.prototype.firstOrDefault = function (selector) {
        for (var i = 0; i < this.length; i++) {
            if (selector(this[i])) return this[i]
        }
        return null
    }
}