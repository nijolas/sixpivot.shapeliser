﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using SixPivot.Shapeliser.Core;
using System;
using System.Collections.Generic;

namespace SixPivot.Shapeliser.Web.Controllers
{
    [Route("api/[controller]")]
    public class ExecuteController : Controller
    {
        private readonly IMemoryCache memoryCache;
        private readonly ITokeniser tokeniser;
        private readonly IParser parser;

        public ExecuteController(IMemoryCache memoryCache, ITokeniser tokeniser, IParser parser)
        {
            this.memoryCache = memoryCache;
            this.tokeniser = tokeniser;
            this.parser = parser;
        }

        // GET api/execute
        [HttpGet]
        public ParsingResult Get(string command)
        {
            return Parse(command);
        }

        private ParsingResult Parse(string command)
        {
            ParsingResult parsingResult;

            if (!memoryCache.TryGetValue(command, out parsingResult))
            {
                var tokens = tokeniser.Tokenise(command);
                parsingResult = parser.Parse(tokens.GetEnumerator());

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(60));

                memoryCache.Set(command, parsingResult, cacheEntryOptions);
            }

            return parsingResult;
        }
    }
}
