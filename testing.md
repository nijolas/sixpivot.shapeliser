﻿# Testing

The test project SixPivot.Shapeliser.Tests contains all tests across the solution.

## Unit tests

The unit tests are split across folders/namespaces in the test project that represent the project and namespace of the units being tested, e.g.

```
namespace: SixPivot.Shapeliser.Core
test namespace: SixPivot.Shapeliser.Tests.Core

namespace: SixPivot.Shapeliser.Web.Controllers
test namespace: SixPivot.Shapeliser.Tests.Web.Controllers
```

Tests for a given unit, i.e. class, should remain in a single file named to match the file/unit/class it tests, using the form [ClassName]Tests.cs e.g.

```
class: SixPivot.Shapeliser.Web.Controllers.ShapeController
file: /SixPivot.Shapeliser.Web/Controllers/ShapeController.cs

test class: SixPivot.Shapeliser.Tests.Web.Controllers.ShapeControllerTests
file: /SixPivot.Shapeliser.Tests/Web/Controllers/ShapeControllerTests.cs
```

### Test class structure

A class may have a number of dependencies injected via its `constructor` arguments. In the test class, these are listed as `private` fields at the top of the class and are named conventionally but with the suffix `Mock` .

The (concrete) unit under test is then listed as another `private` field, seperated from the dependencies.

The class should contain a single `void Initialise` method. This method should mock any dependencies, and then construct the unit under test, e.g.

Unit test classes should be marked with a `[TestCategory("Unit tests")]` attribute.

```
[TestClass]
public class ExampleClassTests
{
	private IDependency1 dependency1Mock;
	private IDependency2 dependency2Mock;

	private ExampleClass exampleClass;

	[TestInitialise]
	public void Initialise()
	{
		dependency1Mock = /* mock */;
		dependency2Mock = /* mock */;

		exampleClass = new ExampleClass(dependency1Mock, dependency2Mock);
	}
}
```

Test methods are then free to simply describe the situation they are testing, without fear of nulls or obfuscating the purpose of the test with unnecessary footwork.

### Test naming and style conventions

* Test methods should follow the naming convention `[Method]_[Condition]_[Expectation]`, e.g.

The method HelloWorld() is expected to produce a string with the text "Hello World":
```
public void HelloWorld_ReturnsHelloWorld()
```

The method Sum(int a, int b) is expected to produce the sum of two integers:
```
public void Sum_GivenNegativeA_AndPositiveB_ReturnsCorrectSum()
```

The method ThrowIfEmpty(string test) is expected to throw an InvalidOperationException if `test` is the empty string:
```
public void ThrowIfEmpty_GivenEmptyString_ThrowsInvalidOperationException()
```

Constructor tests should use the word `Constructor` in place of a method name.

* Tests should clearly seperate and denote the steps of AAA with a single comment line, e.g.

```
public void Sum_GivenNegativeA_AndPositiveB_ReturnsCorrectSum()
{
	// arrange
	/* setup mock expectations, stubs, etc. */

	// act
	/* call the method under test */

	// assert
	/* evaluate the result and that expectations were met, etc. */
}
```

* Assertions should be supplied a contextual, informative message as an argument, e.g.

```
public void Sum_Given5And2_Returns7()
{
	/* ... */
	Assert.AreEqual(7, result, $"5+2=7, not {result}!");
}
```

* Common assert functionality within a single test class should be combined into a function (right under the `void Intialise()` method) following the naming convention `Assert_[Assertion]`, e.g.

```
void Assert_ExampleClassIsNotNullAndHasNoErrorsStartingWithX(ExampleClass class)
{
	/* assert statements */
}

ConcreteType Assert_IsConcreteType(IInterfaceType item)
{
	/* assert statements */
}
```

* Common assert functionality across multiple test classes should be placed in the class `SixPivot.Shapeliser.Tests.AssertCommon` using the same naming convention

## Integration tests

Each top-level, assembly folder should have a folder `_IntegrationTests` where integration tests should be placed. Each class should test a *single* action/path through a group of units.

Test names should be descriptive within the context of the action being performed, but should not refer to any particular classes/methods directly.

Integration test classes should be marked with a `[TestCategory("Integration tests")]` attribute.