﻿using System.Collections.Generic;

namespace SixPivot.Shapeliser.Core
{
    public interface IExpression
    {
        /// <summary>
        /// Parses an expression from the given token enumerator.
        /// </summary>
        /// <param name="tokenEnumerator">The token enumerator to parse from.</param>
        /// <returns>A ParsingResult containing the results of the parse.</returns>
        ParsingResult Parse(IEnumerator<Token> tokenEnumerator);
    }
}
