﻿namespace SixPivot.Shapeliser.Core
{
    public class Token
    {
        private readonly string token;
        private readonly int position;

        internal Token(string token, int position)
        {
            this.token = token;
            this.position = position;
        }

        public string Value => token;

        public int Position => position;
    }
}
