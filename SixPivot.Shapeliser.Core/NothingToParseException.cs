﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SixPivot.Shapeliser.Core
{
    [Serializable]
    public class NothingToParseException : Exception
    {
        public NothingToParseException()
        {
        }

        public NothingToParseException(string message) : base(message)
        {
        }

        public NothingToParseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NothingToParseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
