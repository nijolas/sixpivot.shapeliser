﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SixPivot.Shapeliser.Core
{
    public interface IParser
    {
        ParsingResult Parse(IEnumerator<Token> tokenEnumerator);
    }

    /// <summary>
    /// Parses a series of tokens and returns a expression object
    /// that describes the sentence.
    /// </summary>
    /// <remarks>
    /// This parser looks up what type of expression it's producing
    /// based on the first token. It then offloads the rest of the
    /// parsing to that object.
    /// 
    /// This is a form of top-down parsing, which in this case allows
    /// for a simpler design that bottom-up parsing.
    /// </remarks>
    public class Parser : IParser
    {
        private readonly IExpressionFactory expressionFactory;

        /// <summary>
        /// Parses expressions registered with the provided ExpressionFactory.
        /// </summary>
        /// <param name="expressionFactory">The ExpressionFactory with registered expressions.</param>
        public Parser(IExpressionFactory expressionFactory)
        {
            this.expressionFactory = expressionFactory;
        }

        public ParsingResult Parse(IEnumerator<Token> tokenEnumerator)
        {
            if (tokenEnumerator == null || !tokenEnumerator.MoveNext()) throw new NothingToParseException();

            var firstToken = tokenEnumerator.Current;
            if (firstToken == null) throw new NothingToParseException();

            return ParseExpression(firstToken, tokenEnumerator);
        }

        private ParsingResult ParseExpression(Token expressionToken, IEnumerator<Token> tokenEnumerator)
        {
            try
            {
                var expression = this.expressionFactory.BuildExpression(expressionToken.Value);
                tokenEnumerator.Reset(); // reset and let the expression take over
                return expression.Parse(tokenEnumerator);
            }
            catch (ExpressionNotRegisteredException)
            {
                var error = new SyntaxError($"The expression '{expressionToken.Value}' was not recognised.", expressionToken.Position);
                return new ParsingResult(null, new SyntaxError[] { error });
            }
            catch
            {
                // be sure to rethrow unhandled exceptions
                throw;
            }
        }
    }
}
