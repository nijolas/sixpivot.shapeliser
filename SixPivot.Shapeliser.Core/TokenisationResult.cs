﻿using System.Collections.Generic;

namespace SixPivot.Shapeliser.Core
{
    internal class TokenisationResult
    {
        private readonly IEnumerable<Token> tokens;
        private readonly IEnumerable<SyntaxError> errors;

        public TokenisationResult(IEnumerable<Token> tokens, IEnumerable<SyntaxError> errors)
        {
            this.tokens = tokens;
            this.errors = errors;
        }

        public IEnumerable<Token> Tokens => tokens;

        public IEnumerable<SyntaxError> Errors => errors;
    }
}
