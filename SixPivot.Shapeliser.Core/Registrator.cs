﻿using Microsoft.Extensions.DependencyInjection;
using SixPivot.Shapeliser.Core.Expressions;

namespace SixPivot.Shapeliser.Core
{
    public static class Registrator
    {
        static ExpressionFactory expressionFactory;

        public static void ConfigureServices(IServiceCollection services)
        {
            expressionFactory = new ExpressionFactory();

            expressionFactory.RegisterExpression("draw", (expressionFactory) => new DrawExpression(expressionFactory));
            expressionFactory.RegisterExpression("shape", (expressionFactory) => new ShapeExpression(expressionFactory));
            expressionFactory.RegisterExpression("dimension", (expressionFactory) => new DimensionExpression());

            services.AddSingleton<IExpressionFactory>(expressionFactory);
            services.AddSingleton<ITokeniser, Tokeniser>();
            services.AddSingleton<IParser, Parser>();
        }
    }
}
