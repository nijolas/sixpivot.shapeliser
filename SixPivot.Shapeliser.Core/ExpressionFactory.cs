﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SixPivot.Shapeliser.Core
{
    public interface IExpressionFactory
    {
        /// <summary>
        /// Registers an expression with the factory by its name
        /// </summary>
        /// <typeparam name="TExpression">The type that implements this expression.</typeparam>
        /// <param name="expressionKey">The key by which this expression is indexed.</param>
        /// <param name="constructor">The constructor function that produces the required expression type.</param>
        void RegisterExpression<TExpression>(string expressionKey, Func<IExpressionFactory, TExpression> constructor) where TExpression : class, IExpression;

        /// <summary>
        /// Builds an expression by its name.
        /// </summary>
        /// <param name="expressionKey">The name of the expression.</param>
        /// <returns>A concrete instance of the correct expression.</returns>
        IExpression BuildExpression(string expressionKey);
    }

    public class ExpressionFactory : IExpressionFactory
    {
        // keeps track of all the constructor methods
        private readonly Dictionary<string, Func<IExpressionFactory, IExpression>> constructorList;

        public ExpressionFactory()
        {
            this.constructorList = new Dictionary<string, Func<IExpressionFactory, IExpression>>();
        }

        public void RegisterExpression<TExpression>(string expressionKey, Func<IExpressionFactory, TExpression> constructor)
             where TExpression : class, IExpression
        {
            if (constructorList.ContainsKey(expressionKey))
                throw new ExpressionAlreadyRegisteredException($"The type '{expressionKey}' has already been registered.");

            this.constructorList.Add(expressionKey, (expressionFactory) => constructor(expressionFactory));
        }

        public IExpression BuildExpression(string expressionKey)
        {
            if (this.constructorList.Count == 0)
                throw new NoExpressionsRegisteredException("You must register at least one Expression with the ExpressionFactory via the RegisterExpression<TExpression> method.");

            if (!this.constructorList.ContainsKey(expressionKey))
                throw new ExpressionNotRegisteredException($"The expression '{expressionKey}' is not registered with the ExpressionFactory.");

            var expressionConstructor = this.constructorList[expressionKey];
            return expressionConstructor(this);
        }
    }

    [Serializable]
    public class ExpressionAlreadyRegisteredException : Exception
    {
        public ExpressionAlreadyRegisteredException()
        {
        }

        public ExpressionAlreadyRegisteredException(string message) : base(message)
        {
        }

        public ExpressionAlreadyRegisteredException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExpressionAlreadyRegisteredException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class NoExpressionsRegisteredException : Exception
    {
        public NoExpressionsRegisteredException()
        {
        }

        public NoExpressionsRegisteredException(string message) : base(message)
        {
        }

        public NoExpressionsRegisteredException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoExpressionsRegisteredException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class ExpressionNotRegisteredException : Exception
    {
        public ExpressionNotRegisteredException()
        {
        }

        public ExpressionNotRegisteredException(string message) : base(message)
        {
        }

        public ExpressionNotRegisteredException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExpressionNotRegisteredException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
