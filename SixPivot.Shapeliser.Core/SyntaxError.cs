﻿namespace SixPivot.Shapeliser.Core
{
    public class SyntaxError
    {
        // for use when the token position is unknown
        // because the input has run out
        public const int EOF = -1;

        private readonly string message;
        private readonly int position;
        private readonly ErrorLevel level;

        public SyntaxError(string message, int position, ErrorLevel level = ErrorLevel.Error)
        {
            this.message = message;
            this.position = position;
            this.level = level;
        }

        public string Message => message;

        public int Position => position;

        public ErrorLevel Level => level;
    }

    public enum ErrorLevel
    {
        Information = 0,
        Warning,
        Error
    }
}
