﻿using System.Collections.Generic;
using System.Linq;

namespace SixPivot.Shapeliser.Core
{
    public class ParsingResult
    {
        private readonly IExpression expression;
        private readonly List<SyntaxError> errors;

        internal ParsingResult(IExpression expression, IEnumerable<SyntaxError> outerErrors, IEnumerable<SyntaxError> innerErrors = null)
        {
            this.expression = expression;

            errors = (outerErrors ?? Enumerable.Empty<SyntaxError>()).ToList();
            errors.AddRange(innerErrors ?? Enumerable.Empty<SyntaxError>());
        }

        public IExpression Expression => expression;

        public IEnumerable<SyntaxError> Errors => errors;
    }
}
