﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace SixPivot.Shapeliser.Core
{
    public interface ITokeniser
    {
        IEnumerable<Token> Tokenise(string input);
    }

    internal class Tokeniser : ITokeniser
    {
        // Chosen arbitrarily as the max length of a Tweet
        private const int MAX_INPUT_LENGTH = 280;

        private List<Token> tokens;

        private int currentTokenStartPosition;
        private int currentPosition;

        private StringBuilder tokenBuilder;
        private TokeniserState state;

        public IEnumerable<Token> Tokenise(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return Enumerable.Empty<Token>();
            if (input.Length > MAX_INPUT_LENGTH) throw new InputTooLongException($"The maximum input is {MAX_INPUT_LENGTH}, but the input received was {input.Length} long.");

            // add a space at the end as an EOF
            // so we don't have to do special handling
            // at the end of the loop
            input += ' ';

            // ensure the last result gets cleared
            ResetState();

            for (currentPosition = 0; currentPosition < input.Length; currentPosition++)
            {
                var currentChar = input[currentPosition];

                switch (state)
                {
                    case TokeniserState.NotInToken:
                        HandleNotInToken(currentChar);
                        break;

                    case TokeniserState.InToken:
                        HandleInToken(currentChar);
                        break;

                    default:
                        throw new InvalidTokeniserStateException($"Toknesier state {state} unrecognised.");
                }
            }

            return this.tokens;
        }

        private void HandleNotInToken(char currentChar)
        {
            if (IsAlpha(currentChar) || IsNumeric(currentChar))
            {
                this.state = TokeniserState.InToken;
                this.currentTokenStartPosition = this.currentPosition;
                this.tokenBuilder.Append(currentChar);
            }
        }

        private void HandleInToken(char currentChar)
        {
            if (IsAlpha(currentChar) || IsNumeric(currentChar))
            {
                this.tokenBuilder.Append(currentChar);
            }
            else
            {
                this.state = TokeniserState.NotInToken;
                this.tokens.Add(new Token(this.tokenBuilder.ToString(), this.currentTokenStartPosition));
                this.tokenBuilder.Clear();
            }
        }

        private void ResetState()
        {
            this.tokens = new List<Token>();

            this.currentTokenStartPosition = 0;
            this.currentPosition = 0;

            this.tokenBuilder = new StringBuilder();
            this.state = TokeniserState.NotInToken;
        }

        static bool IsAlpha(char character)
        {
            return ((character >= 'a' && character <= 'z') || (character >= 'A' && character <= 'Z'));
        }

        static bool IsNumeric(char character)
        {
            return (character >= '0' && character <= '9');
        }

        enum TokeniserState
        {
            NotInToken = 0,
            InToken
        }
    }

    [Serializable]
    public class InputTooLongException : Exception
    {
        public InputTooLongException()
        {
        }

        public InputTooLongException(string message) : base(message)
        {
        }

        public InputTooLongException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InputTooLongException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class InvalidTokeniserStateException : Exception
    {
        public InvalidTokeniserStateException()
        {
        }

        public InvalidTokeniserStateException(string message) : base(message)
        {
        }

        public InvalidTokeniserStateException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidTokeniserStateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
