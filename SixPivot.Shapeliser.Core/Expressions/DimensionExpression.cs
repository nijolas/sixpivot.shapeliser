﻿using System.Collections.Generic;
using System.Linq;

namespace SixPivot.Shapeliser.Core.Expressions
{
    public class DimensionExpression : IExpression
    {
        private const string EXPECTED_DIMENSION_TYPE_ERROR = "Expected a dimension type, e.g. 'radius'.";
        private const string EXPECTED_DIMENSION_VALUE_ERROR = "Expected a dimension value, e.g. 6";

        private List<SyntaxError> syntaxErrors;

        private Token dimensionType;
        private Token dimensionValue;

        public Token DimensionType => dimensionType;
        public Token DimensionValue => dimensionValue;

        internal DimensionExpression()
        {
            syntaxErrors = new List<SyntaxError>();
        }

        ParsingResult IExpression.Parse(IEnumerator<Token> tokenEnumerator)
        {
            if (tokenEnumerator == null) throw new NothingToParseException();

            if (!tokenEnumerator.MoveNext()) return EofError(EXPECTED_DIMENSION_TYPE_ERROR);

            // sentence structure [with/and] [a/an] (type) [of] (value)
            if (!SkipIfFound(tokenEnumerator, "with", "and")) return EofError(EXPECTED_DIMENSION_TYPE_ERROR);
            if (!SkipIfFound(tokenEnumerator, "a", "an")) return EofError(EXPECTED_DIMENSION_TYPE_ERROR);

            // next token should be the dimension type
            dimensionType = tokenEnumerator.Current;
            if (!tokenEnumerator.MoveNext()) return EofError(EXPECTED_DIMENSION_VALUE_ERROR);

            // check for "of"
            if (!SkipIfFound(tokenEnumerator, "of")) return EofError(EXPECTED_DIMENSION_VALUE_ERROR);

            // next token should be the dimension value
            dimensionValue = tokenEnumerator.Current;

            return new ParsingResult(this, syntaxErrors);
        }

        private ParsingResult EofError(string message)
        {
            syntaxErrors.Add(new SyntaxError(message, SyntaxError.EOF));
            return new ParsingResult(null, syntaxErrors);
        }

        private bool SkipIfFound(IEnumerator<Token> tokenEnumerator, params string[] words)
        {
            return words.Contains(tokenEnumerator.Current.Value) ?
                tokenEnumerator.MoveNext() :
                true;
        }
    }
}