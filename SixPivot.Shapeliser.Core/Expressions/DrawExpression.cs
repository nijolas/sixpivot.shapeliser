﻿using System.Collections.Generic;

namespace SixPivot.Shapeliser.Core.Expressions
{
    public class DrawExpression : IExpression
    {
        private IExpressionFactory expressionFactory;

        private ShapeExpression shapeExpression;
        private List<SyntaxError> syntaxErrors;

        public ShapeExpression ShapeExpression => shapeExpression;

        internal DrawExpression(IExpressionFactory expressionFactory)
        {
            this.expressionFactory = expressionFactory;
            syntaxErrors = new List<SyntaxError>();
        }

        ParsingResult IExpression.Parse(IEnumerator<Token> tokenEnumerator)
        {
            if (tokenEnumerator == null || !tokenEnumerator.MoveNext()) throw new NothingToParseException();
            
            // we expect the first few tokens to describe the
            // shape we want to draw, e.g. "an isoceles triangle", "a circle" or just "rectangle"
            var token = tokenEnumerator.Current;

            if (token.Value != "draw")
            {
                // this should never happen, but check anyway
                syntaxErrors.Add(new SyntaxError("Expected command 'draw'.", token.Position, ErrorLevel.Error));
                return new ParsingResult(null, syntaxErrors);
            }

            // parse the shape
            var expression = expressionFactory.BuildExpression("shape");
            shapeExpression = (expression as ShapeExpression);
            var result = expression.Parse(tokenEnumerator);
            return new ParsingResult(this, syntaxErrors, result.Errors);
        }
    }
}
