﻿using System;
using System.Collections.Generic;

namespace SixPivot.Shapeliser.Core.Expressions
{
    public class ShapeExpression : IExpression
    {
        private readonly IExpressionFactory expressionFactory;

        private Token shapeToken;
        private List<DimensionExpression> dimensionExpressions;
        private List<SyntaxError> syntaxErrors;

        public Token ShapeToken => shapeToken;
        public List<DimensionExpression> DimensionExpressions => dimensionExpressions;

        internal ShapeExpression(IExpressionFactory expressionFactory)
        {
            this.expressionFactory = expressionFactory;
            dimensionExpressions = new List<DimensionExpression>();
            syntaxErrors = new List<SyntaxError>();
        }

        ParsingResult IExpression.Parse(IEnumerator<Token> tokenEnumerator)
        {
            if (tokenEnumerator == null) throw new NothingToParseException();

            if (!tokenEnumerator.MoveNext())
            {
                syntaxErrors.Add(new SyntaxError("Expected a shape expression.", SyntaxError.EOF));
                return new ParsingResult(null, syntaxErrors);
            }

            // check for the indefinite article
            var token = tokenEnumerator.Current;
            if (token.Value != "a" && token.Value != "an")
            {
                syntaxErrors.Add(new SyntaxError("Missing indefinite article 'a' or 'an'.", token.Position, ErrorLevel.Warning));
            }
            else
            {
                if (!tokenEnumerator.MoveNext())
                {
                    syntaxErrors.Add(new SyntaxError("Expected a shape expression.", SyntaxError.EOF));
                    return new ParsingResult(null, syntaxErrors);
                }
            }

            // save the shape name
            shapeToken = tokenEnumerator.Current;

            // dimensions consume the rest of the tokens
            var result = new ParsingResult(this, syntaxErrors);
            do
            {
                var dimensionExpression = expressionFactory.BuildExpression("dimension");
                var dimensionResult = dimensionExpression.Parse(tokenEnumerator);
                result = new ParsingResult(this, result.Errors, dimensionResult.Errors);
                if (dimensionResult.Expression != null) dimensionExpressions.Add(dimensionResult.Expression as DimensionExpression);
            }
            while (tokenEnumerator.MoveNext());

            return result;
        }
    }
}
